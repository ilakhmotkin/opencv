package com.machinevision.numberplate

import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.SurfaceView
import android.view.WindowManager
import kotlinx.android.synthetic.main.activity_camera.*
import org.opencv.android.CameraBridgeViewBase
import org.opencv.android.InstallCallbackInterface
import org.opencv.android.LoaderCallbackInterface
import org.opencv.android.OpenCVLoader
import org.opencv.core.*
import org.opencv.features2d.DescriptorExtractor
import org.opencv.features2d.DescriptorMatcher
import org.opencv.features2d.FeatureDetector
import org.opencv.imgproc.Imgproc
import java.io.IOException
import java.util.*


class CameraActivity : AppCompatActivity(), CameraBridgeViewBase.CvCameraViewListener2, ActivityCompat.OnRequestPermissionsResultCallback, LoaderCallbackInterface {
    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    external fun stringFromJNI(): String

    companion object {

        private const val TAG = "CameraActivity"

        private const val REQUEST_CAMERA = 0

        init {
            System.loadLibrary("native-lib")
            System.loadLibrary("opencv_java3")
            if (!OpenCVLoader.initDebug())
                Log.d(TAG, "Unable to load OpenCV")
            else
                Log.d(TAG, "OpenCV loaded")
        }
    }

    private var w: Int = 0

    private var h: Int = 0
    var RED = Scalar(255.0, 0.0, 0.0)
    var GREEN = Scalar(0.0, 255.0, 0.0)
    var detector: FeatureDetector? = null
    var descriptor: DescriptorExtractor? = null
    var matcher: DescriptorMatcher? = null
    var descriptors2: Mat? = null
    var descriptors1:Mat? = null
    var keypoints1: MatOfKeyPoint? = null
    var keypoints2:MatOfKeyPoint? = null

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        openCVCameraView.visibility = SurfaceView.VISIBLE
        openCVCameraView.setCvCameraViewListener(this)
    }

//    fun showCameraView(){
//            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
//                    == PackageManager.PERMISSION_GRANTED) {
//                startCamera()
//            } else {
//                Log.i(TAG, "CAMERA permission has already been granted. Displaying camera preview.")
//            }
//    }

    private fun recognize(aInputFrame: Mat?): Mat {

        Imgproc.cvtColor(aInputFrame, aInputFrame, Imgproc.COLOR_RGB2GRAY)
        descriptors2 = Mat()
        keypoints2 = MatOfKeyPoint()
        detector?.detect(aInputFrame, keypoints2)
        descriptor?.compute(aInputFrame, keypoints2, descriptors2)

        // Matching
        val matches = MatOfDMatch()
        val matchesList = matches.toList()

        var maxDist: Double = 0.0
        var minDist: Double = 100.0

        for (i in matchesList.indices) {
            val dist = matchesList[i].distance.toDouble()
            if (dist < minDist)
                minDist = dist
            if (dist > maxDist)
                maxDist = dist
        }

        val good_matches = LinkedList<DMatch>()
        for (i in matchesList.indices) {
            if (matchesList[i].distance <= 1.5 * minDist)
                good_matches.addLast(matchesList[i])
        }

        val goodMatches = MatOfDMatch()
        goodMatches.fromList(good_matches)
        val outputImg = Mat()
        val drawnMatches = MatOfByte()
//        aInputFrame?.let {
//            if (aInputFrame.empty() || aInputFrame.cols() < 1 || aInputFrame.rows() < 1) {
//                return aInputFrame
//            }
//            Imgproc.resize(outputImg, outputImg, aInputFrame.size())
//        }

        return aInputFrame!!
    }

    override fun onResume() {
        super.onResume()
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization")
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, this)
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!")
            onManagerConnected(LoaderCallbackInterface.SUCCESS)
        }
    }

//    private fun startCamera() {
//        openCVCameraView.visibility = SurfaceView.VISIBLE
//        openCVCameraView.setCvCameraViewListener(this)
//    }

    @Throws(IOException::class)
    private fun initializeOpenCVDependencies() {
        openCVCameraView?.enableView()
        detector = FeatureDetector.create(FeatureDetector.ORB)
        descriptor = DescriptorExtractor.create(DescriptorExtractor.ORB)
        matcher = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE_HAMMING)

        descriptors1 = Mat()
        keypoints1 = MatOfKeyPoint()
    }

    override fun onCameraViewStarted(width: Int, height: Int) {
        w = width
        h = height
    }

    override fun onCameraViewStopped() {

    }

    override fun onManagerConnected(status: Int) {
        when (status) {
            LoaderCallbackInterface.SUCCESS -> {
                Log.i(TAG, "OpenCV loaded successfully")
                openCVCameraView.enableView()
                try {
                    initializeOpenCVDependencies()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
            else -> {
                return
            }
        }
    }

    override fun onPackageInstall(operation: Int, callback: InstallCallbackInterface?) {

    }

    override fun onCameraFrame(inputFrame: CameraBridgeViewBase.CvCameraViewFrame?): Mat {
        return recognize(inputFrame?.rgba())
    }

    override fun onPause() {
        super.onPause()
        if (openCVCameraView != null)
            openCVCameraView.disableView()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (openCVCameraView != null)
            openCVCameraView.disableView()
    }
}

package com.machinevision.numberplate

import android.annotation.TargetApi
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.SurfaceView
import android.view.WindowManager
import android.widget.SeekBar
import kotlinx.android.synthetic.main.activity_main.*
import org.opencv.android.BaseLoaderCallback
import org.opencv.android.CameraBridgeViewBase
import org.opencv.android.LoaderCallbackInterface
import org.opencv.android.OpenCVLoader
import org.opencv.core.CvType
import org.opencv.core.Mat
import org.opencv.imgproc.Imgproc

class MainActivity : AppCompatActivity(), CameraBridgeViewBase.CvCameraViewListener2 {
    external fun ConvertRGBtoGray(matAddrInput: Long, matAddrResult: Long)

    external fun ConvertRGBtoHCV(matAddrInput: Long, matAddrResult: Long)

    companion object {

        private val TAG = "opencv"

        init {
            System.loadLibrary("opencv_java3")
            System.loadLibrary("native-lib")
        }
        internal val PERMISSIONS_REQUEST_CODE = 1000

    }

    internal var PERMISSIONS = arrayOf("android.permission.CAMERA")

    private var seekProgress: Int = 120
        set(value) {
            valueSeekLabel.text = value.toString()
            field = value
        }

    private var seekProgress2: Int = 100
        set(value) {
            valueSeekLabel2.text = value.toString()
            field = value
        }

    private val mLoaderCallback = object : BaseLoaderCallback(this) {
        override fun onManagerConnected(status: Int) {
            when (status) {
                LoaderCallbackInterface.SUCCESS -> {
                    openCVCamera?.enableView()
                }
                else -> {
                    super.onManagerConnected(status)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        window.setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        setContentView(R.layout.activity_main)

        cameraPermissions()
        setupCVCamera()

        mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS)

        valuesSeek.progress = seekProgress
        valuesSeek.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, p1: Int, p2: Boolean) {
                seekBar?.let {
                    seekProgress = it.progress
                }
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {
            }

            override fun onStopTrackingTouch(p0: SeekBar?) {
            }

        })
        valueSeekLabel.text = seekProgress.toString()
        valuesSeek2.progress = seekProgress2
        valuesSeek2.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, p1: Int, p2: Boolean) {
                seekBar?.let {
                    seekProgress2 = it.progress
                }
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {
            }

            override fun onStopTrackingTouch(p0: SeekBar?) {
            }
        })
        valueSeekLabel2.text = seekProgress2.toString()
    }

    private fun setupCVCamera() {
        openCVCamera?.visibility = SurfaceView.VISIBLE
        openCVCamera?.setCvCameraViewListener(this)
        openCVCamera?.setCameraIndex(0)
        openCVCamera?.enableFpsMeter()
    }

    private fun cameraPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!hasPermissions(PERMISSIONS)) {
                requestPermissions(PERMISSIONS, PERMISSIONS_REQUEST_CODE)
            }
        }
    }

    public override fun onPause() {
        super.onPause()
        openCVCamera?.disableView()
    }

    public override fun onResume() {
        super.onResume()

        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "onResume :: Internal OpenCV library not found.")
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_2_0, this, mLoaderCallback)
        } else {
            Log.d(TAG, "onResum :: OpenCV library found inside package. Using it!")
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS)
        }
    }

    public override fun onDestroy() {
        super.onDestroy()
        openCVCamera?.disableView()
    }

    private lateinit var mat1: Mat
    private lateinit var mat2: Mat
    private lateinit var mat3: Mat

    override fun onCameraViewStarted(width: Int, height: Int) {
        Log.d(TAG, "onCameraViewStarted. w:${width} h: ${height}")
        mat1 = Mat(width, height, CvType.CV_8UC4)
        mat2 = Mat(width, height, CvType.CV_8UC4)
        mat3 = Mat(width, height, CvType.CV_8UC4)
    }

    override fun onCameraViewStopped() {
        mat1.release()
        mat2.release()
        mat3.release()

    }


    override fun onCameraFrame(inputFrame: CameraBridgeViewBase.CvCameraViewFrame): Mat {

        var matInput = inputFrame.rgba()

        val grey = Mat()
        Imgproc.cvtColor(matInput, grey, Imgproc.COLOR_BGR2GRAY)
//        Imgproc.adaptiveThreshold(grey, grey, seekProgress.toDouble(), Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C,
//                Imgproc.THRESH_BINARY, seekProgress2, 1.0)
        Imgproc.Canny(grey,grey,seekProgress.toDouble(),seekProgress2.toDouble())

        return grey
    }

    private fun hasPermissions(permissions: Array<String>): Boolean {
        var result: Int

        for (perms in permissions) {

            result = ContextCompat.checkSelfPermission(this, perms)

            if (result == PackageManager.PERMISSION_DENIED) {
                return false
            }
        }

        return true
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {

            PERMISSIONS_REQUEST_CODE -> if (grantResults.size > 0) {
                val cameraPermissionAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED

                if (!cameraPermissionAccepted)
                    showDialogForPermission("Allow camera")
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private fun showDialogForPermission(msg: String) {
        val builder = AlertDialog.Builder(this@MainActivity)
        builder.setTitle("Camera permission")
        builder.setMessage(msg)
        builder.setCancelable(false)
        builder.setPositiveButton("Ok") { dialog, id -> requestPermissions(PERMISSIONS, PERMISSIONS_REQUEST_CODE) }
        builder.setNegativeButton("Cancel") { arg0, arg1 -> finish() }
        builder.create().show()
    }
}